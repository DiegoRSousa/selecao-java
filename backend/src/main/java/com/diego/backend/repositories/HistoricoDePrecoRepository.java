package com.diego.backend.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.diego.backend.models.HistoricoDePreco;

@Repository
public interface HistoricoDePrecoRepository extends JpaRepository<HistoricoDePreco, Long> {

	@Query(value = "select cast(avg(valor_de_venda) as decimal(10, 3)) from historico_de_preco where municipio = :municipio",
			nativeQuery = true)
	Double getMediaDePrecoPorMunicipio(@Param("municipio") String municipio);
	
	@Query(value = "select h.id, h.regiao, h.estado, h.municipio, h.revenda, h.codigo, h.produto, h.data_coleta, "
			+ "h.valor_de_compra, h.valor_de_venda, h.unidade_de_medida, h.bandeira from historico_de_preco h "
			+ "where h.regiao = 'NE'", nativeQuery = true)
	List<HistoricoDePreco> getPorRegiao(@Param("regiao") String regiao);
	
	@Query(value = "select h.id, h.regiao, h.estado, h.municipio, h.revenda, h.codigo, h.produto, h.data_coleta, " 
			+ " h.valor_de_compra, h.valor_de_venda, h.unidade_de_medida, h.bandeira from historico_de_preco h "
			+ "order by revenda", nativeQuery = true)
	List<HistoricoDePreco> getDadosAgrupadosPorDistribuidora();
	
	@Query(value = "select h.id, h.regiao, h.estado, h.municipio, h.revenda, h.codigo, h.produto, h.data_coleta, " 
			+ " h.valor_de_compra, h.valor_de_venda, h.unidade_de_medida, h.bandeira from historico_de_preco h "
			+ "order by data_coleta", nativeQuery = true) 
	List<HistoricoDePreco> getDadosAgrupadosPorDataDaColeta();
	
	@Query(value = "select cast(avg(valor_de_compra) as decimal(10, 3)) from historico_de_preco where municipio = :municipio", nativeQuery = true)
	Double getValorMedioDeCompraPorMunicipio(@Param("municipio") String municipio);
	
	@Query(value = "select cast(avg(valor_de_venda) as decimal(10,3)) from historico_de_preco "
			+ "where municipio = :municipio", nativeQuery = true)
	Double getValorMedioDeVendaPorMunicipio(@Param("municipio") String municipio);
	
	@Query(value = "select cast(avg(valor_de_compra) as decimal(10, 3)) from historico_de_preco "
			+ "where bandeira = :bandeira", nativeQuery = true)
	Double getValorMedioDeCompraPorBandeira(@Param("bandeira") String bandeira);
	
	@Query(value = "select cast(avg(valor_de_venda) as decimal(10, 3)) from historico_de_preco "
			+ "where bandeira = :bandeira", nativeQuery = true)
	Double getValorMedioDeVendaPorBandeira(@Param("bandeira") String bandeira);
}