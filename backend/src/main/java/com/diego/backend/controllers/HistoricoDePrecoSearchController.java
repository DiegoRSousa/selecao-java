package com.diego.backend.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.diego.backend.models.HistoricoDePreco;
import com.diego.backend.services.HistoricoDePrecoService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("historicos/search")
public class HistoricoDePrecoSearchController {
	
	@Autowired
	private HistoricoDePrecoService historicoDePrecoService;

	@ApiOperation("Média de preço de combustível com base no nome do município")
	@GetMapping(value = "/mediadeprecopormunicipio/{municipio}")
	public ResponseEntity<Double> getMediaDePrecoPorMunicipio(@PathVariable String municipio) {
		return ResponseEntity.ok(historicoDePrecoService.getMediaDePrecoPorMunicipio(municipio));
	}
	
	@ApiOperation("Todas as informações importadas por sigla da região")
	@GetMapping(value = "/porregiao/{regiao}")
	public ResponseEntity<List<HistoricoDePreco>> getPorRegiao(@PathVariable String regiao) {
		return ResponseEntity.ok(historicoDePrecoService.getPorRegiao(regiao));
	}
	
	@ApiOperation("Dados agrupados por distribuidora")
	@GetMapping(value = "/agrupadosposrdistribuidora")
	public ResponseEntity<List<HistoricoDePreco>> getDadosAgrupadosPorDistribuidora() {
		return ResponseEntity.ok(historicoDePrecoService.getDadosAgrupadosPorDistribuidora());
	}
	
	@ApiOperation("Dados agrupados pela data da coleta")
	@GetMapping(value = "/agrupadospordata")
	public ResponseEntity<List<HistoricoDePreco>> getDadosAgrupadosPorDataDaColeta() {
		return ResponseEntity.ok(historicoDePrecoService.getDadosAgrupadosPorDataDaColeta());
	}
	
	@ApiOperation("Valor médio do valor da compra por município")
	@GetMapping(value = "/valormediodecomprapormunicipio/{municipio}")
	public ResponseEntity<Double> getValorMedioDeCompraPorMunicipio(@PathVariable String municipio) {
		return ResponseEntity.ok(historicoDePrecoService.getValorMedioDeCompraPorMunicipio(municipio));
	}
	
	@ApiOperation("Valor médio do valor da venda por município")
	@GetMapping(value = "/valormediodevendapormunicipio/{municipio}")
	public ResponseEntity<Double> getValorMedioDeVendaPorMunicipio(@PathVariable String municipio) {
		return ResponseEntity.ok(historicoDePrecoService.getValorMedioDeVendaPorMunicipio(municipio));
	}
	
	@ApiOperation("Valor médio do valor da compra por bandeira")
	@GetMapping(value = "/valormediodecompraporbandeira/{bandeira}")
	public ResponseEntity<Double> getValorMedioDeCompraPorBandeira(@PathVariable String bandeira) {
		return ResponseEntity.ok(historicoDePrecoService.getValorMedioDeCompraPorBandeira(bandeira));
	}
	
	@ApiOperation("Valor médio do valor da venda por bandeira")
	@GetMapping(value = "/valormediodevendaporbandeira/{bandeira}")
	public ResponseEntity<Double> getValorMedioDeVendaPorBandeira(@PathVariable String bandeira) {
		return ResponseEntity.ok(historicoDePrecoService.getValorMedioDeVendaPorBandeira(bandeira));
	}
}
