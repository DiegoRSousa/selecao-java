package com.diego.backend.controllers;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.diego.backend.dto.HistoricoDePrecoDTO;
import com.diego.backend.models.HistoricoDePreco;
import com.diego.backend.services.HistoricoDePrecoService;

@CrossOrigin
@RestController
@RequestMapping("historicos")
public class HistoricoDePrecoController {

	@Autowired
	private HistoricoDePrecoService historicoDePrecoService;
	
	@GetMapping
	public ResponseEntity<List<HistoricoDePreco>> findAll() {
		List<HistoricoDePreco> historicos = historicoDePrecoService.findAll();
		return ResponseEntity.ok(historicos);
	}
	
	@GetMapping("/page")
	public ResponseEntity<Page<HistoricoDePreco>> findPage(
				@RequestParam(value = "page", defaultValue = "0") Integer page,
				@RequestParam(value = "linesPerPage", defaultValue = "24") Integer linesPerPage,
				@RequestParam(value = "direction", defaultValue = "ASC") String direction,
				@RequestParam(value = "orderBy", defaultValue = "id") String orderBy) {
		Page<HistoricoDePreco> historicos = historicoDePrecoService.findPage(page, linesPerPage, direction, orderBy);
		return ResponseEntity.ok(historicos);
	}
	
	@GetMapping("{id}")
	public ResponseEntity<HistoricoDePreco> find(@PathVariable Long id) {
		HistoricoDePreco historicoDePreco = historicoDePrecoService.find(id);
		return ResponseEntity.ok(historicoDePreco);
	}
	
	@PostMapping
	public ResponseEntity<HistoricoDePreco> save(@Valid @RequestBody HistoricoDePrecoDTO historicoDePrecoDTO) {
		HistoricoDePreco historicoDePreco = historicoDePrecoService.fromDTO(historicoDePrecoDTO);
		historicoDePreco = historicoDePrecoService.save(historicoDePreco);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
				.path("/{id}").buildAndExpand(historicoDePreco.getId()).toUri();
		return ResponseEntity.created(uri).build();
	}
	
	@PutMapping("{id}")
	public ResponseEntity<HistoricoDePreco> update(@Valid @RequestBody HistoricoDePrecoDTO historicoDePrecoDTO, 
					@PathVariable Long id) {
		HistoricoDePreco historicoDePreco = historicoDePrecoService.fromDTO(historicoDePrecoDTO);
		historicoDePreco.setId(id);
		historicoDePreco = historicoDePrecoService.update(historicoDePreco);
		return ResponseEntity.ok(historicoDePreco);
	}
	
	@DeleteMapping("{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		historicoDePrecoService.delete(id);
		return ResponseEntity.noContent().build();
	}
}