package com.diego.backend.controllers.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter @AllArgsConstructor
public class StandardError {

	private Long timeStamp;
	private Integer status;
	private String error;
	private String message;
	private String path;
}
