package com.diego.backend.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.diego.backend.services.UploadService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("upload")
public class UploadController {

	@Autowired
	UploadService uploadService;
	
	@PostMapping
	@ApiOperation("Selecione o arquivo .csv para upload")
	public ResponseEntity<Void> upload(@RequestPart("file") MultipartFile file) {
		uploadService.upload(file);
		return ResponseEntity.ok().build();
	}
}