package com.diego.backend.dto;

import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class UsuarioDTO {

	@NotEmpty(message = "Preenchimento obrigatório")
	@Length(max=240, message = "Tamanho máximo 240 caracteres")
	private String nome;
	@NotEmpty(message = "Preenchimento obrigatório")
	@Length(max=240, message = "Tamanho máximo 240 caracteres")
	private String senha;
}
