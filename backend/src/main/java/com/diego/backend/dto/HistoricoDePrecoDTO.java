package com.diego.backend.dto;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class HistoricoDePrecoDTO {

	@NotEmpty(message = "Preenchimento obrigatório")
	@Length(min = 1, max = 2, message = "o campo regiao deve conter entre 1 e 2 caracteres")
	private String regiao;
	@NotEmpty(message = "Preenchimento obrigatório")
	@Length(min = 2, max = 2, message = "o campo estado deve conter 2 caracteres")
	private String estado;
	@NotEmpty(message = "Preenchimento obrigatório")
	@Length(max=240, message = "Tamanho máximo 240 caracteres")
	private String municipio;
	@NotEmpty(message = "Preenchimento obrigatório")
	@Length(max=240, message = "Tamanho máximo 240 caracteres")
	private String revenda;
	@NotEmpty(message = "Preenchimento obrigatório")
	@Length(max=6, message = "Tamanho máximo 6 caracteres")
	private String codigo;
	@NotEmpty(message = "Preenchimento obrigatório")
	@Length(max=240, message = "Tamanho máximo 240 caracteres")
	private String produto;
	@Temporal(TemporalType.DATE)
	private Date dataColeta;
	@Min(value = 0, message = "O valor não pode ser negativo")
	private double valorDeCompra;
	@Min(value = 0, message = "O valor não pode ser negativo")
	private double valorDeVenda;
	@NotEmpty(message = "Preenchimento obrigatório")
	@Length(max=240, message = "Tamanho máximo 243 caracteres")
	private String unidadeDeMedida;
	@NotEmpty(message = "Preenchimento obrigatório")
	@Length(max=240, message = "Tamanho máximo 244 caracteres")
	private String bandeira;
}
