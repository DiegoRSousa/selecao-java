package com.diego.backend.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.diego.backend.dto.HistoricoDePrecoDTO;
import com.diego.backend.models.HistoricoDePreco;
import com.diego.backend.repositories.HistoricoDePrecoRepository;
import com.diego.backend.services.exception.ObjectNotFoundException;

@Service
public class HistoricoDePrecoService {

	@Autowired
	private HistoricoDePrecoRepository historicoDePrecoRepository;
	
	public List<HistoricoDePreco> findAll() {
		return historicoDePrecoRepository.findAll();
	}
	
	public Page<HistoricoDePreco> findPage(Integer page, Integer linesPerPage, String direction, String orderBy) {
		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);
		return historicoDePrecoRepository.findAll(pageRequest);
	}
	
	public HistoricoDePreco find(Long id) {
		Optional<HistoricoDePreco> historicoDePreco = historicoDePrecoRepository.findById(id);
		return historicoDePreco.orElseThrow(() -> new ObjectNotFoundException(id, HistoricoDePreco.class.getName()));
	}
	
	public HistoricoDePreco save(HistoricoDePreco historicoDePreco) {
		return historicoDePrecoRepository.save(historicoDePreco);
	}
	
	public void saveAll(List<HistoricoDePreco> historicosDePreco) {
		historicoDePrecoRepository.saveAll(historicosDePreco);
	}
	
	public HistoricoDePreco update(HistoricoDePreco historicoDePreco) {
		find(historicoDePreco.getId());
		return historicoDePrecoRepository.save(historicoDePreco);
	}
	
	public void delete(Long id) {
		find(id);
		historicoDePrecoRepository.deleteById(id);
	}
	
	public Double getMediaDePrecoPorMunicipio(String municipio) {
		return historicoDePrecoRepository.getMediaDePrecoPorMunicipio(municipio);
	}
	
	public List<HistoricoDePreco> getPorRegiao(String regiao) {
		return historicoDePrecoRepository.getPorRegiao(regiao);
	}
	
	public List<HistoricoDePreco> getDadosAgrupadosPorDistribuidora() {
		return historicoDePrecoRepository.getDadosAgrupadosPorDistribuidora();
	}
	
	public List<HistoricoDePreco> getDadosAgrupadosPorDataDaColeta() {
		return historicoDePrecoRepository.getDadosAgrupadosPorDataDaColeta();
	}
	
	public Double getValorMedioDeCompraPorMunicipio(String municipio) {
		return historicoDePrecoRepository.getValorMedioDeCompraPorMunicipio(municipio);
	}
	
	public Double getValorMedioDeVendaPorMunicipio(String municipio) {
		return historicoDePrecoRepository.getValorMedioDeVendaPorMunicipio(municipio);
	}
	
	public Double getValorMedioDeCompraPorBandeira(String bandeira) {
		return historicoDePrecoRepository.getValorMedioDeCompraPorBandeira(bandeira);
	}
	
	public Double getValorMedioDeVendaPorBandeira(String bandeira) {
		return historicoDePrecoRepository.getValorMedioDeVendaPorBandeira(bandeira);
	}
	
	public HistoricoDePreco fromDTO(HistoricoDePrecoDTO historicoDePrecoDTO) {
		return new HistoricoDePreco(null, historicoDePrecoDTO.getRegiao(), historicoDePrecoDTO.getEstado(), 
						historicoDePrecoDTO.getMunicipio(), historicoDePrecoDTO.getRevenda(), 
						historicoDePrecoDTO.getCodigo(), historicoDePrecoDTO.getProduto(), 
						historicoDePrecoDTO.getDataColeta(), historicoDePrecoDTO.getValorDeCompra(),
						historicoDePrecoDTO.getValorDeVenda(), historicoDePrecoDTO.getUnidadeDeMedida(), 
						historicoDePrecoDTO.getBandeira());
		
	}
}
