package com.diego.backend.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.diego.backend.dto.UsuarioDTO;
import com.diego.backend.models.Usuario;
import com.diego.backend.repositories.UsuarioRepository;
import com.diego.backend.services.exception.ObjectNotFoundException;

@Service
public class UsuarioService {

	@Autowired
	private UsuarioRepository usuarioRepository;
	
	public List<Usuario> findAll() {
		return usuarioRepository.findAll();
	}
	
	public Usuario find(Long id) {
		Optional<Usuario> usuario = usuarioRepository.findById(id);
		return usuario.orElseThrow(() -> new ObjectNotFoundException(id, Usuario.class.getName()));
	}
	
	public Usuario save(Usuario usuario) {
		return usuarioRepository.save(usuario);
	}
	
	public Usuario update(Usuario usuario) {
		this.find(usuario.getId());
		return usuarioRepository.save(usuario);
	}
	
	public void delete(Long id) {
		this.find(id);
		usuarioRepository.deleteById(id);
	}

	public Usuario fromDTO(UsuarioDTO usuarioDTO) {
		return new Usuario(null, usuarioDTO.getNome(), usuarioDTO.getSenha());
	}
}