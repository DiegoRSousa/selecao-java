package com.diego.backend.services.exception;

public class InvalidFileException extends RuntimeException{

	private static final long serialVersionUID = 1L;
	
	public InvalidFileException(String msg) {
		super(msg);
	}
}
