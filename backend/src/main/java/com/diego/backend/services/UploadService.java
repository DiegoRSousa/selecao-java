package com.diego.backend.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.diego.backend.models.HistoricoDePreco;
import com.diego.backend.services.exception.InvalidFileException;

@Service
public class UploadService {
	
	@Autowired
	private HistoricoDePrecoService historicoDePrecoService;
	
	public void upload(MultipartFile file) {
		if(!file.getOriginalFilename().contains("csv"))
			throw new InvalidFileException("A extenção do arquivo deve ser *.csv");
		 
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(file.getInputStream()));
			String linha = "";
			String separador = "  ";
			List<HistoricoDePreco> historicos = new ArrayList<HistoricoDePreco>();
			
			while((linha = reader.readLine()) != null) {
				if(linha.startsWith("Regi"))
					continue;
				String[] campo = linha.split(separador);
				if(campo.length != 11)
					continue;
				if(campo[7].isEmpty())
					campo[7] = "0";
				
				System.out.println(campo[0]);
				
				historicos.add(new HistoricoDePreco(null, campo[0], campo[1].trim(), 
						campo[2], campo[3], campo[4], campo[5], 
						new SimpleDateFormat("dd/MM/yyyy").parse(campo[6]), Double.parseDouble(campo[7].replace(',', '.')), 
						Double.parseDouble(campo[8].replace(',', '.')), campo[9], campo[10]));
			}
			
			System.out.println("salvando");
			historicoDePrecoService.saveAll(historicos);
			System.out.println("finalizado");
			
		} catch (IOException | NumberFormatException | ParseException e) {
			e.printStackTrace();
		}       
	}
}