package com.diego.backend.services.exception;

public class ObjectNotFoundException extends RuntimeException{

	private static final long serialVersionUID = 1L;
	
	public ObjectNotFoundException(Long id, String clazz) {
		super("Objeto não encontrado! Id: " + id + ", Tipo: " + clazz);
	}

}
