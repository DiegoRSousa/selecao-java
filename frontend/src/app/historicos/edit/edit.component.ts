import { Component, OnInit } from '@angular/core';
import { RequestCreate } from '../shared/historico';
import { HistoricoService } from '../shared/historico.service';
import { HistoricoDataService } from '../shared/historico-data.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  id: string;

  request: RequestCreate = {
    regiao: '',
    estado: '',
    municipio: '',
    revenda: '',
    codigo: '',
    produto: '',
    dataColeta: '',
    valorDeCompra: 0,
    valorDeVenda: 0,
    unidadeDeMedida:  '',
    bandeira:  '',
  }
  
  constructor(private historicoService: HistoricoService, private historicoDataService: HistoricoDataService) { }

  ngOnInit() {
    this.historicoDataService.currentHistorico.subscribe(data => {
      if(data.regiao && data.id) {
        this.request.regiao = data.regiao
        this.request.estado = data.estado
        this.request.municipio = data.municipio
        this.request.revenda = data.revenda
        this.request.codigo = data.codigo
        this.request.produto = data.produto
        this.request.dataColeta = data.dataColeta
        this.request.valorDeCompra = data.valorDeCompra
        this.request.valorDeVenda = data.valorDeVenda
        this.request.unidadeDeMedida = data.unidadeDeMedida
        this.request.bandeira = data.bandeira
        this.id = data.id
      }
    });
  }

  onSubmit(){
    if(this.id) {
      this.historicoService.update(this.id, this.request).subscribe(res => {
        alert('Cadastro salvo com sucesso');
        this.ngOnInit();
      });
    } else {
      this.historicoService.create(this.request).subscribe(res => {
        alert('Cadastro realizado com sucesso!');
        this.ngOnInit();
      });
    }
  }
}
