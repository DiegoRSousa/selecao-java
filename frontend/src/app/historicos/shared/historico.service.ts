import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ResponseHistoricos, Historico, RequestCreate, ResponseUpdate } from './historico';

@Injectable({
  providedIn: 'root'
})
export class HistoricoService {

  private url = "http://localhost:8080/historicos"

  constructor(private http: HttpClient) { }

  getAll(): Observable<ResponseHistoricos> {
    return this.http.get<ResponseHistoricos>(`${this.url}/page`);
  }

  get(id: string) : Observable<Historico> {
    return this.http.get<Historico>(`${this.url}/id`);
  }

  create(request: RequestCreate) : Observable<any> {
    return this.http.post<any>(this.url, request);
  }

  update(id: string, request: RequestCreate) : Observable<ResponseUpdate> {
    return this.http.put<ResponseUpdate>(`${this.url}/${id}`, request);
  }

  delete(id: string) : Observable<any> {
    return this.http.delete(`${this.url}/${id}`);
  }
}
