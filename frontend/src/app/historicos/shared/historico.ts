export interface Historico {
    id: number;
    regiao: string;
    estado: string;
    municipio: string;
    revenda: string;
    codigo: string;
    produto: string;
    dataColeta: string;
    valorDeCompra: number;
    valorDeVenda: number;
    unidadeDeMedida: string;
    bandeira: string;
}

export interface Sort {
    sorted: boolean;
    unsorted: boolean;
    empty: boolean;
}

export interface Pageable {
    sort: Sort;
    offset: number;
    pageNumber: number;
    pageSize: number;
    paged: boolean;
    unpaged: boolean;
}

export interface ResponseHistoricos {
    content: Historico[];
    pageable: Pageable;
    totalPages: number;
    last: boolean;
    totalElements: number;
    size: number;
    number: number;
    sort: Sort;
    numberOfElements: number;
    first: boolean;
    empty: boolean;
}

export interface RequestCreate {
    regiao: string;
    estado: string;
    municipio: string;
    revenda: string;
    codigo: string;
    produto: string;
    dataColeta: string;
    valorDeCompra: number;
    valorDeVenda: number;
    unidadeDeMedida: string;
    bandeira: string;
}

export interface ResponseUpdate {
    id: BigInteger
    regiao: string;
    estado: string;
    municipio: string;
    revenda: string;
    codigo: string;
    produto: string;
    dataColeta: string;
    valorDeCompra: number;
    valorDeVenda: number;
    unidadeDeMedida: string;
    bandeira: string;
}