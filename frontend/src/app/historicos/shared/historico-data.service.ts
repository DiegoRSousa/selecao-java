import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Historico } from './historico';

@Injectable({
  providedIn: 'root'
})

export class HistoricoDataService {

  private historicoSource 
      = new BehaviorSubject({
        regiao: null,
        estado: null,
        municipio: null,
        revenda: null,
        codigo: null,
        produto: null,
        dataColeta: null,
        valorDeCompra: 0.0,
        valorDeVenda: 0.0,
        unidadeDeMedida: null,
        bandeira: null,
        id: '',});
  currentHistorico = this.historicoSource.asObservable();
  constructor() { }

  changeHistorico(historico: Historico, id: string) {
    this.historicoSource.next({regiao: historico.regiao, estado: historico.estado, 
          municipio: historico.municipio, revenda: historico.revenda, codigo: historico.codigo,
          produto: historico.produto, dataColeta: historico.dataColeta, 
          valorDeCompra: historico.valorDeCompra, valorDeVenda: historico.valorDeVenda,
          unidadeDeMedida: historico.unidadeDeMedida, bandeira: historico.bandeira, id: id});
  }


}
