import { Component, OnInit } from '@angular/core';
import { ResponseHistoricos, Historico } from '../shared/historico';
import { HistoricoService } from '../shared/historico.service';
import { HistoricoDataService } from '../shared/historico-data.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  
  responseHistoricos :ResponseHistoricos;

  constructor(private historicoService : HistoricoService, 
    private historicoDataService: HistoricoDataService) { }

  ngOnInit() {
    this.historicoService.getAll()
      .subscribe(res => this.responseHistoricos = res);  
  }

  edit(historico: Historico, id: string) {
    this.historicoDataService.changeHistorico(historico, id);
  }

  delete(id: string) {
    this.historicoService.delete(id).subscribe(res => {
      alert('Removido com sucesso!');
      this.ngOnInit();
    });
  }

}
